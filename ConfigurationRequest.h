Configuration Request Data Structures
-------------------------------------

//auto exposure mode control
//#define AUTO_EXPOSURE_MODE_MAN			0x01
#define AUTO_EXPOSURE_MODE_AUTO			0x02	//<- use this
//#define AUTO_EXPOSURE_MODE_SHUTTER		0x04
//#define AUTO_EXPOSURE_MODE_APERTURE		0x08

//auto exposure priority control
#define AUTO_EXPOSURE_PRIO_CONST		0x00 	//<- use this. Means that the frame rate must remain constant
//#define AUTO_EXPOSURE_PRIO_DYN			0x01 	

//exposure time absolute
//do not use this control if the AUTO_EXPOSURE_MODE_AUTO is set. Default case
//#define EXPOSURE_TIME_ABS				0x0000 	//some value between 1(0.0001 sec) and 100 000 (10 sec) aperture

//focus auto control 
//#define AUTO_FOCUS_OFF					0x00
#define AUTO_FOCUS_ON					0x01 	//<- use this

//stream negotiation probe control USB Revision 1.5 
//set_cur probe control 
uint8_t wLength = 48; 
typedef struct _Video_Probe_Ctrl
{
	uint8_t requ_type = 0x21; 
	uint8_t request = SET_CUR; 
	uint16_t ctl_sel = VS_PROBE_CONTROL; 
	uint16_t index = 0x0001;  							//Video streaming interface index
	uint16_t wLength = 0x30; 

	//data fields
	uint16_t bmHint; 									//Dunno yet maybe keep the frame interval constant. Let's see for the others
	uint8_t bFormatIndex = 0x03; 						//Video Streaming MJPEG Format Type Descriptor
	uint8_t bFrameIndex = 0x01; 						//Video Streaming MJPEG Frame Type Descriptor 640x480
	uint32_t bFrameInterval = 0x00051615 				//33.333300 mSec (30.00 Hz)
	uint16_t wKeyFrameRate = 0x0000; 					//not supported by the webcam 
	uint16_t wPFrameRate = 0x0000; 						//not supported by the webcam
	uint16_t wCompQuality = 0x01F4; 					//set to 500 (middle value) provisionally -> let's see what the GET_DEF value is
	uint16_t wCompWindowSize = 0x0000; 					//not supported by the webcam
	uint16_t wDelay = 0x0000; 							//read by the host 
	uint32_t dwMaxVideoFrameSize = 0x00000000;			//read by the host should be 0x00096000 default
	uint32_t dwMaxPayloadTransferSize = 0x00000000;		//read by the host 
	uint32_t dwClockFrequency = 0x11E1A300; 			//300'000'000 Hz
	uint8_t bmFramingInfo = 0x00; 						//read only by the host
	uint8_t bPreferedVersion = 0x00; 					//init to zero on probe set
	uint8_t	bMinVersion = 0x00; 						//init to zero on probe set
	uint8_t bMaxVersion = 0x00; 						//init to zero on probe set
	uint8_t bUsage = 0x00; 								//does not seem to be supported
	uint8_t	bBitDepthLuma = 
	uint8_t bmSettings = 0x00; 							//not supported
	uint8_t	bMaxNumberOfRefFramesPlus1 = 0x00; 			//does not seem to be supported
	uint16_t bmRateControlModes = 0x0000; 				//not supported
	uint64_t bmLayoutPerStream = 0x0000000000000000;	//not supported
}

//stream negotiation probe control USB Revision 1.1
//set_cur probe control 
uint8_t wLength = 34;  
typedef struct _Video_Probe_Ctrl
{
	uint8_t requ_type = 0x21; 
	uint8_t request = SET_CUR; 
	uint16_t ctl_sel = VS_PROBE_CONTROL; 
	uint16_t index = 0x0001;  						//Video streaming interface index
	uint16_t wLength = 0x22; 

	//data fields
	uint16_t bmHint; 								//Dunno yet maybe keep the frame interval constant. Let's see for the others
	uint8_t bFormatIndex = 0x03; 					//Video Streaming MJPEG Format Type Descriptor
	uint8_t bFrameIndex = 0x01; 					//Video Streaming MJPEG Frame Type Descriptor 640x480
	uint32_t bFrameInterval = 0x00051615 			//33.333300 mSec (30.00 Hz)
	uint16_t wKeyFrameRate = 0x0000; 				//not supported by the webcam 
	uint16_t wPFrameRate = 0x0000; 					//not supported by the webcam
	uint16_t wCompQuality = 0x01F4; 				//set to 500 (middle value) provisionally -> let's see what the GET_DEF value is
	uint16_t wCompWindowSize = 0x0000; 				//not supported by the webcam
	uint16_t wDelay = 0x0000; 						//read by the host 
	uint32_t dwMaxVideoFrameSize = 0x00000000;		//read by the host should be 0x00096000 default
	uint32_t dwMaxPayloadTransferSize = 0x00000000;	//read by the host 
	uint32_t dwClockFrequency = 0x11E1A300; 		//300'000'000 Hz
	uint8_t bmFramingInfo = 0x00; 					//read only by the host
	uint8_t bPreferedVersion = 0x00; 				//init to zero on probe set
	uint8_t	bMinVersion = 0x00; 					//init to zero on probe set
	uint8_t bMaxVersion = 0x00; 					//init to zero on probe set
}

//stream negotiation commit control
/* TODO maybe needs a structure to put the read data from the device into dunno yet*/
