/*
 * StaticFunctions.c
 *
 *  Created on: 13 juin 2018
 *      Author: Colin Cina
 */

#include "../../CAM/Inc/usbh_cam.h"
#include "../../CAM/Inc/usbh_cam_def.h"

/** @addtogroup USBH_LIB
 * @{
 */

/** @addtogroup USBH_LIB_CLASS
 * @{
 */

/** @addtogroup USBH_LIB_CLASS_CAM
 * @{
 */

/** @defgroup USBH_CAM_Exported_Functions
 * @brief This file is includes USB descriptors
 * @{
 */





/**
 * @}
 */

/**
 * @}
 */

/**
 * @}
 */

/**
 * @}
 */
