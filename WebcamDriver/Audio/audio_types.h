/*
 * audio_types.h
 *
 *  Created on: 1 Aug 2018
 *      Author: adamt
 */

#ifndef AUDIO_TYPES_H_
#define AUDIO_TYPES_H_

#include "stdio.h"

// Type defintions
typedef uint32_t audio_length;
typedef int16_t audio_data;

#endif /* AUDIO_TYPES_H_ */
